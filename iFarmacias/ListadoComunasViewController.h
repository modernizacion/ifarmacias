//
//  ListadoComunasViewController.h
//  iFarmacias
//
//  Created by Nicolas Silva on 04-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ListadoComunasViewController;

@protocol ListadoComunasViewControllerDelegate <NSObject>
- (void)ListadoComunasViewController:(ListadoComunasViewController *)controller didSelectComuna:(NSDictionary *)comuna;
@end

@interface ListadoComunasViewController : UITableViewController

@property (nonatomic, weak) id <ListadoComunasViewControllerDelegate> delegate;
@property(nonatomic,strong)NSDictionary *selectedRegion;
@property(nonatomic,strong)NSDictionary *selectedComuna;

@end
