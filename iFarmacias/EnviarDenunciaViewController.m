//
//  EnviarDenunciaViewController.m
//  iFarmacias
//
//  Created by Nicolas Silva on 14-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "EnviarDenunciaViewController.h"
#import <Parse/Parse.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "MSTextView.h"

@interface EnviarDenunciaViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nombreCompletoTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet MSTextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;
@property (weak, nonatomic) IBOutlet UILabel *direccionLabel;
@property (weak, nonatomic) IBOutlet UILabel *horarioLabel;
- (IBAction)cancelButtonTouched:(id)sender;
- (IBAction)sendButtonTouched:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) UITextView *activeTextView;
@property (nonatomic,strong) UIImage *photoImage;
@property (weak, nonatomic) IBOutlet UIButton *tomarFotoButton;

- (IBAction)tomarFotoTouched:(id)sender;

- (void)registerForKeyboardNotifications;
- (void)keyboardWasShown:(NSNotification*)aNotification;
- (void)keyboardWillBeHidden:(NSNotification*)aNotification;

-(void)focusActiveField;

- (BOOL)validateEmailWithString:(NSString*)email;

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate;
- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate;

@end

@implementation EnviarDenunciaViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self registerForKeyboardNotifications];
    self.textView.placeholder=@"Indique la hora y las observaciones que crea necesarias.";
    self.nombreLabel.text=[NSString stringWithFormat:@"Farmacia %@",self.farmacia.nombre];
    self.direccionLabel.text=self.farmacia.direccion;
    self.horarioLabel.text=[NSString stringWithFormat:@"Abierto de %@ a %@",self.farmacia.horarioApertura,self.farmacia.horarioCierre];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)cancelButtonTouched:(id)sender{
    [self.delegate enviarDenunciaViewControllerDidCancel:self];
}

-(void)sendButtonTouched:(id)sender{
    if (self.nombreCompletoTextField.text.length==0) {
        [[[UIAlertView alloc] initWithTitle:@"Falta información" message:@"Debe escribir su nombre completo." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        return;
    }
    if (self.emailTextField.text.length==0) {
        [[[UIAlertView alloc] initWithTitle:@"Falta información" message:@"Debe escribir correo electrónico." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        return;
    }
    if (![self validateEmailWithString:self.emailTextField.text]) {
        [[[UIAlertView alloc] initWithTitle:@"Falta información" message:@"Debe escribir correo electrónico válido." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        return;
    }
    if (self.textView.text.length==0) {
        [[[UIAlertView alloc] initWithTitle:@"Falta información" message:@"Debe escribir observaciones para incluir en la denuncia." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        return;
    }
    
    
    
    PFObject *object = [PFObject objectWithClassName:@"Denuncia"];
    [object setObject:self.farmacia.comuna forKey:@"comuna"];
    [object setObject:self.farmacia.nombre forKey:@"nombre"];
    [object setObject:self.farmacia.direccion forKey:@"direccion"];
    [object setObject:self.nombreCompletoTextField.text forKey:@"nombreDenunciante"];
    [object setObject:self.emailTextField.text forKey:@"emailDenunciante"];
    [object setObject:self.textView.text forKey:@"observaciones"];
    
    if (self.photoImage) {
        NSData *imageData = UIImageJPEGRepresentation(self.photoImage,0.8);
        PFFile *imageFile = [PFFile fileWithName:@"image.jpg" data:imageData];
        [object setObject:imageFile forKey:@"foto"];
    }
    
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            [SVProgressHUD showSuccessWithStatus:@"Enviado"];
        }else{
            [SVProgressHUD showErrorWithStatus:@"No se pudo enviar"];
        }
    }];
    
    [self.delegate enviarDenunciaViewControllerDidSend:self];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    [self focusActiveField];
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    //CGRect aRect = self.view.frame;
    //aRect.size.height -= kbSize.height;
    //if (!CGRectContainsPoint(aRect, CGPointMake(self.activeField.frame.origin.x, self.activeField.frame.origin.y + self.scrollView.contentInset.top + 40) ) ) {
    //    [self.scrollView scrollRectToVisible:self.activeField.frame animated:YES];
    //}

}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.scrollView.contentInset.top, 0.0, 0.0, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.activeTextView = textView;
    [self focusActiveField];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.activeTextView = nil;
}

-(void)focusActiveField{
    //CGRect aRect = self.view.frame;
    //aRect.size.height -= kbSize.height;
    //if (!CGRectContainsPoint(aRect, CGPointMake(self.activeField.frame.origin.x, self.activeField.frame.origin.y + self.scrollView.contentInset.top + 40) ) ) {
        [self.scrollView scrollRectToVisible:self.activeTextView.frame animated:YES];
    //}
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (IBAction)tomarFotoTouched:(id)sender {
    UIActionSheet *actionSheet=[[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Tomar foto",@"Seleccionar foto", nil];
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {   //Tomar foto
        if (![self startCameraControllerFromViewController:self usingDelegate:self]) {
            UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"No hay camara disponible" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alertView show];
        }
    }else if (buttonIndex==1){
        if (![self startMediaBrowserFromViewController:self usingDelegate:self]) {
            UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:@"No hay fotos disponibles" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alertView show];
        }
    }
}

- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose picture or
    // movie capture, if both are available:
    //cameraUI.mediaTypes =
    //[UIImagePickerController availableMediaTypesForSourceType:
    // UIImagePickerControllerSourceTypeCamera];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = YES;
    
    cameraUI.delegate = delegate;
    
    [controller presentViewController:cameraUI animated:YES completion:nil];
    return YES;
}

- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    // Displays saved pictures and movies, if both are available, from the
    // Camera Roll album.
    //mediaUI.mediaTypes =
    //[UIImagePickerController availableMediaTypesForSourceType:
    // UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = NO;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI animated:YES completion:nil];
    return YES;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *originalImage, *editedImage, *imageToSave;
    
    // Handle a still image capture

        
        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];
        
        if (editedImage) {
            imageToSave = editedImage;
        } else {
            imageToSave = originalImage;
        }
        
        // Save the new image (original or edited) to the Camera Roll
        //UIImageWriteToSavedPhotosAlbum (imageToSave, nil, nil , nil);
    self.photoImage=imageToSave;
    
    self.tomarFotoButton.enabled=NO;
    self.tomarFotoButton.backgroundColor=[UIColor grayColor];
    self.tomarFotoButton.titleLabel.text=@"Foto capturada";
    
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
