//
//  MSDate.m
//  iFarmacias
//
//  Created by Nicolas Silva on 11-11-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "NSDate+Extension.h"

@implementation NSDate(Extension)

+(instancetype)chileanLabourDate{
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    calendar.timeZone=[NSTimeZone timeZoneWithName:@"America/Santiago"];
    NSDateComponents *components = [calendar components:NSHourCalendarUnit | NSMinuteCalendarUnit fromDate:[NSDate date]];
    
    NSDate *date=[NSDate date];
    if (([components hour]) < 8) {
        NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
        offsetComponents.day=-1;
        date=(NSDate *)[calendar dateByAddingComponents:offsetComponents toDate:[NSDate date] options:0];
    }
    
    return date;
}

@end
