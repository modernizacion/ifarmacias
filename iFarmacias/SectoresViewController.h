//
//  ComunasViewController.h
//  iFarmacias
//
//  Created by Nicolas Silva on 03-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListadoRegionesViewController.h"
#import "ListadoComunasViewController.h"

@interface SectoresViewController : UITableViewController <ListadoRegionesViewControllerDelegate,ListadoComunasViewControllerDelegate>

@end
