//
//  FarmaciaTable.m
//  iFarmacias
//
//  Created by Nicolas Silva on 10-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "FarmaciaTable.h"
#import "Farmacia.h"

@interface FarmaciaTable()

@end

@implementation FarmaciaTable



+(void)findFarmaciasWithBlock:(void (^)(NSArray *, NSError *))block{
    
    NSURLSession *session = [NSURLSession sharedSession];
    [[session dataTaskWithURL:[NSURL URLWithString:@"http://farmanet.minsal.cl/maps/index.php/ws/getLocales"] completionHandler:^(NSData *data,NSURLResponse *response,NSError *error) {
        // handle response
        NSMutableArray *farmacias=[[NSMutableArray alloc] init];
        NSArray *json=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        //NSLog(@"%@",json);
        for (NSDictionary *f in json) {
            Farmacia *farmacia=[[Farmacia alloc] init];
            farmacia.nombre=f[@"local_nombre"];
            farmacia.coordenadas=CLLocationCoordinate2DMake([f[@"local_lat"] doubleValue], [f[@"local_lng"] doubleValue]);
            farmacia.direccion=f[@"local_direccion"];
            farmacia.telefono=f[@"local_telefono"];
            farmacia.horarioApertura=f[@"funcionamiento_hora_apertura"];
            farmacia.horarioCierre=f[@"funcionamiento_hora_cierre"];
            farmacia.comuna=f[@"comuna_nombre"];
            
            [farmacias addObject:farmacia];
        }
        if(block){  //Como el block del NSURLSession corre en un background thread, tenemos que lanzar este block al main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                block(farmacias,error);
            });
        }
        
    }] resume];
    
    
}



+(void)findFarmaciasByComuna:(NSString *)nombre withBlock:(void (^)(NSArray *, NSError *))block{
    [self findFarmaciasWithBlock:^(NSArray *farmacias, NSError *error) {
        NSPredicate *predicate=[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            BOOL result=NO;
            Farmacia *f=evaluatedObject;
            NSString *unaccentedNombre = [nombre stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale localeWithLocaleIdentifier:@"es_CL"]];
            NSString *unaccentedNombre2 = [f.comuna stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale localeWithLocaleIdentifier:@"es_CL"]];
            if ([unaccentedNombre.lowercaseString  isEqualToString:unaccentedNombre2.lowercaseString]) {
                result = YES;
            }
            
            return result;
        }];
        
        NSArray *filteredFarmacias=[farmacias filteredArrayUsingPredicate:predicate];
        
        if(block)block(filteredFarmacias,error);
    }];
}


@end
