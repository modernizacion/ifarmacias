//
//  FarmaciaViewController.m
//  iFarmacias
//
//  Created by Nicolas Silva on 14-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "FarmaciaViewController.h"

@interface FarmaciaViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nombreLabel;
@property (weak, nonatomic) IBOutlet UILabel *direccionLabel;
@property (weak, nonatomic) IBOutlet UILabel *horarioLabel;
@property (weak, nonatomic) IBOutlet UIButton *telefonoButton;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *comoLlegarButton;
@property (weak, nonatomic) IBOutlet UIView *comoLlegarView;

- (IBAction)comoLlegarTouched:(id)sender;
- (IBAction)telefonoTouched:(id)sender;
@end

@implementation FarmaciaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.comoLlegarView.layer.borderColor=[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0].CGColor;
    self.comoLlegarView.layer.borderWidth=0.5;
    
    self.nombreLabel.text=[NSString stringWithFormat:@"Farmacia %@",self.farmacia.nombre];
    self.direccionLabel.text=self.farmacia.direccion;
    self.horarioLabel.text=[NSString stringWithFormat:@"Abierto de %@ a %@",self.farmacia.horarioApertura,self.farmacia.horarioCierre];
    
    [self.telefonoButton setTitle:self.farmacia.telefono forState:UIControlStateNormal];
    
    if (!CLLocationCoordinate2DIsValid(self.farmacia.coordenadas)) {
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(-33.44288, -70.65383), 8000000, 8000000) animated:NO];
        self.comoLlegarButton.hidden=YES;
    }else{
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.farmacia.coordenadas, 1000, 1000)];
        MKPointAnnotation *annotation=[[MKPointAnnotation alloc] init];
        annotation.coordinate=self.farmacia.coordenadas;
        [self.mapView addAnnotation:annotation];
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView*    pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.canShowCallout = NO;
            

        }
        else
            pinView.annotation = annotation;
        
        if (self.farmacia.estaAbierta) {
            pinView.image = [UIImage imageNamed:@"annotation.png"];
        }else{
            pinView.image = [UIImage imageNamed:@"annotation-grayscale.png"];
        }
        
        return pinView;
    }
    
    return nil;
}

-(void)enviarDenunciaViewControllerDidCancel:(EnviarDenunciaViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)enviarDenunciaViewControllerDidSend:(EnviarDenunciaViewController *)controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    EnviarDenunciaViewController *viewController=[[segue destinationViewController] viewControllers][0];
    viewController.delegate=self;
    viewController.farmacia=self.farmacia;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)comoLlegarTouched:(id)sender {
    //MKPlacemark *farmaciaPlacemark=[[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake([self.farmacia[@"lat"] doubleValue], [self.farmacia[@"lng"] doubleValue] ) addressDictionary:nil];
    //farmaciaPlacemark.name=@"Hola";
    
    MKMapItem *farmaciaItem=[[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:self.farmacia.coordenadas addressDictionary:nil]];
    
    farmaciaItem.name=[NSString stringWithFormat:@"Farmacia %@",self.farmacia.nombre];
    //CLLocation* fromLocation = from.placemark.location;
    
    // Create a region centered on the starting point with a 10km span
    //MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(fromLocation.coordinate, 10000, 10000);
    
    // Open the item in Maps, specifying the map region to display.
    [MKMapItem openMapsWithItems:@[[MKMapItem mapItemForCurrentLocation],farmaciaItem] launchOptions:@{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving}];
}

- (IBAction)telefonoTouched:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.farmacia.telefono]]];

}
@end
