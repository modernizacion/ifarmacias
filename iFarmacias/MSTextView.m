//
//  MSTextView.m
//  iFarmacias
//
//  Created by Nicolas Silva on 15-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "MSTextView.h"

@implementation MSTextView

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = [[UIColor colorWithRed:245.0/255.0 green:218.0/255.0 blue:162.0/255.0 alpha:1.0] CGColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
