//
//  MSCell.m
//  iFarmacias
//
//  Created by Nicolas Silva on 15-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "MSCell.h"

@implementation MSCell


-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if (self.accessoryType==UITableViewCellAccessoryDisclosureIndicator) {
            self.accessoryView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
        }
    }
    return self;
}


@end
