//
//  CercanasViewController.m
//  iFarmacias
//
//  Created by Nicolas Silva on 03-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "CercanasViewController.h"
#import "FarmaciaTable.h"
#import "FarmaciaViewController.h"
#import "MSFarmaciaAnnotation.h"
#import "NSDate+Extension.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <OCMapView/OCMapView.h>
#import "ClusterAnnotationView.h"
#import <MKMapViewZoom/MKMapView+ZoomLevel.h>

//#import <ADClusterMapView/ADClusterMapView.h>

@interface CercanasViewController ()
@property (weak, nonatomic) IBOutlet OCMapView *mapView;
@property (nonatomic, strong) CLLocation *userLocation;
@property (nonatomic, strong) NSDate *dateUpdated;
-(void)refreshAnnotations;
-(void)didDragMap:(UIGestureRecognizer*)gestureRecognizer;
@end

@implementation CercanasViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
        

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.panToUserLocation=YES;
    
    self.tabBarController.delegate=self;
    
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self.mapView addGestureRecognizer:panRec];
    
    self.mapView.clusteringMethod=OCClusteringMethodGrid;
    self.mapView.clusterByGroupTag=YES;
    
    //Centramos el mapa
    if (self.centerLocation) {
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.centerLocation.coordinate, 20000, 20000) animated:NO];
    }else{
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(-33.44288, -70.65383), 20000, 20000) animated:NO];
    }
    
    //[self refreshAnnotations];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //Refrescamos solo cuando no ha habido refresco antes, o cuando la ultima actualizacion fue el dia anterior.
    if (self.dateUpdated==nil) {
        [self refreshAnnotations];
    }else{
        NSDateComponents *componentsToday = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate chileanLabourDate]];
        NSDateComponents *componentsUpdated = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:self.dateUpdated];
        if ([componentsToday day]!=[componentsUpdated day]) {
            [self refreshAnnotations];
        }
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if (self.panToUserLocation) {
        self.navigationController.tabBarItem.selectedImage=[UIImage imageNamed:@"tabbar-cercanas.png"];
        self.panToUserLocation=NO;
    }else{
        self.navigationController.tabBarItem.selectedImage=[UIImage imageNamed:@"tabbar-cercanas-locate.png"];
        [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.userLocation.coordinate, 1000, 1000) animated:YES];
        self.panToUserLocation=YES;
    }
    
}

-(void)refreshAnnotations{
    //Cargamos las farmacias
    [SVProgressHUD showWithStatus:@"Cargando farmacias"];
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    [FarmaciaTable findFarmaciasWithBlock:^(NSArray *farmacias, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:@"Problemas en la conexión"];
        }else{
            NSMutableArray *annotationsArray=[[NSMutableArray alloc] init];
            for (Farmacia *f in farmacias) {
                
                if (CLLocationCoordinate2DIsValid(f.coordenadas)) {
                    MSFarmaciaAnnotation *annotation=[[MSFarmaciaAnnotation alloc] init];
                    annotation.coordinate=f.coordenadas;
                    annotation.title=[NSString stringWithFormat:@"Farmacia %@",f.nombre];
                    annotation.subtitle=f.direccion;
                    annotation.farmacia=f;
                    [annotationsArray addObject:annotation];
                }
    
            }
            
            [self.mapView removeAnnotations:self.mapView.annotations];
            [self.mapView addAnnotations:annotationsArray];
            
            self.dateUpdated=[NSDate chileanLabourDate];
            
            
            [SVProgressHUD showSuccessWithStatus:@"Completado"];
        }
    }];
}


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
    if (!self.userLocation || [self.userLocation distanceFromLocation:userLocation.location]>1000) {    //Se requiere actualizar localizacion si no se ha actualizado, o si ha variado mucho.
        if (!self.userLocation || self.panToUserLocation) {     //Movemos el mapa si es la primera vez, o si es que esta la opcion de que el mapa te siga
            [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, 1000, 1000) animated:YES];
        }
        self.userLocation=userLocation.location;    //Actualizamos la localizacion
    }
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isKindOfClass:[OCAnnotation class]]) {
        OCAnnotation *clusterAnnotation=(OCAnnotation *)annotation;
        
        // create your custom cluster annotationView here!
        ClusterAnnotationView *annotationView = (ClusterAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterAnnotationView"];
        
        if (!annotationView)
        {
            // If an existing pin view was not available, create one.
            annotationView = [[ClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterAnnotationView"];
            
            
        }
        else
            annotationView.annotation = clusterAnnotation;
        
        if ([clusterAnnotation.groupTag isEqualToString:@"abierta"]) {
            annotationView.color=[UIColor colorWithRed:3.0/255.0 green:98.0/255.0 blue:183.0/255.0 alpha:0.8];
        }else{
            annotationView.color=[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.8];
        }
        
        annotationView.numero=clusterAnnotation.annotationsInCluster.count;
        
        return annotationView;
    }
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[MSFarmaciaAnnotation class]])
    {
        MSFarmaciaAnnotation *farmaciaAnnotation=(MSFarmaciaAnnotation *)annotation;
        
        // Try to dequeue an existing pin view first.
        MKAnnotationView*    pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"FarmaciaAnnotationView"];
        
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:farmaciaAnnotation
                                                       reuseIdentifier:@"FarmaciaAnnotationView"];
            
            pinView.canShowCallout = YES;
            
            // Add a detail disclosure button to the callout.

            UIButton* rightButton = [UIButton buttonWithType:
                                     UIButtonTypeDetailDisclosure];

            pinView.rightCalloutAccessoryView = rightButton;
            
        }
        else
            pinView.annotation = farmaciaAnnotation;
        
        if (farmaciaAnnotation.farmacia.estaAbierta) {
            pinView.image = [UIImage imageNamed:@"annotation.png"];
        }else{
            pinView.image = [UIImage imageNamed:@"annotation-grayscale.png"];
        }
        
        
        return pinView;
    }
    
    return nil;
}


-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    [self performSegueWithIdentifier:@"Farmacia" sender:view];
}


- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated{
    NSUInteger zoomLevel=[self.mapView zoomLevel];
        
    if (zoomLevel > 16) {
        self.mapView.clusteringEnabled=NO;
    }else{
        self.mapView.clusteringEnabled=YES;
    }
    
    [self.mapView doClustering];
    
    
    
}


-(void)didDragMap:(UIGestureRecognizer*)gestureRecognizer{
    if (self.panToUserLocation) {
        self.navigationController.tabBarItem.selectedImage=[UIImage imageNamed:@"tabbar-cercanas.png"];
        self.panToUserLocation=NO;
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    FarmaciaViewController *viewController=[segue destinationViewController];
    sender=(MKAnnotationView *)sender;
    viewController.farmacia=[[sender annotation] farmacia];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    self.tabBarController.delegate=nil;
    self.mapView.delegate=nil;
}

@end
