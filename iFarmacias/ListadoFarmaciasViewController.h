//
//  ListadoFarmaciasViewController.h
//  iFarmacias
//
//  Created by Nicolas Silva on 03-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListadoFarmaciasViewController : UITableViewController

@property(nonatomic,strong)NSDictionary *region;
@property(nonatomic,strong)NSDictionary *comuna;

@end
