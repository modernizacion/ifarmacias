//
//  EnviarDenunciaViewController.h
//  iFarmacias
//
//  Created by Nicolas Silva on 14-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Farmacia.h"

@class EnviarDenunciaViewController;

@protocol EnviarDenunciaViewControllerDelegate <NSObject>
- (void)enviarDenunciaViewControllerDidCancel:(EnviarDenunciaViewController *)controller;
- (void)enviarDenunciaViewControllerDidSend:(EnviarDenunciaViewController *)controller;
@end


@interface EnviarDenunciaViewController : UIViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate,UITextFieldDelegate>
@property(nonatomic,strong) Farmacia *farmacia;
@property(nonatomic,weak) id <EnviarDenunciaViewControllerDelegate> delegate;
@end
