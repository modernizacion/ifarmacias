//
//  ListadoRegionesViewController.h
//  iFarmacias
//
//  Created by Nicolas Silva on 04-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ListadoRegionesViewController;

@protocol ListadoRegionesViewControllerDelegate <NSObject>
- (void)ListadoRegionesViewController:(ListadoRegionesViewController *)controller didSelectRegion:(NSDictionary *)region;
@end

@interface ListadoRegionesViewController : UITableViewController

@property (nonatomic, weak) id <ListadoRegionesViewControllerDelegate> delegate;
@property(nonatomic,strong)NSDictionary *selectedRegion;

@end
