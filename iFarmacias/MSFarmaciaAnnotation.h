//
//  MSFarmaciaAnnotation.h
//  iFarmacias
//
//  Created by Nicolas Silva on 14-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <OCMapView/OCMapView.h>
#import "Farmacia.h"

@interface MSFarmaciaAnnotation : MKPointAnnotation <OCGrouping>
@property(nonatomic,strong)Farmacia *farmacia;
@end
