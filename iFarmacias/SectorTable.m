//
//  SectorTable.m
//  iFarmacias
//
//  Created by Nicolas Silva on 04-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "SectorTable.h"
#import "Table.h"

@interface SectorTable()
//@property(nonatomic,strong)FMDatabase *db;
@end

@implementation SectorTable

/*
-(id)init{
    self=[super init];
    if (self) {
        NSString *dbPath = [[NSBundle mainBundle] pathForResource:@"ifarmacias" ofType:@"sqlite"];
        
        self.db = [FMDatabase databaseWithPath:dbPath];
        if(![self.db open]){
            NSLog(@"Could not open database.");
            return nil;
        }
    }
    return self;
}
*/
 
+(NSArray *)findRegiones{
    NSMutableArray *regiones=[[NSMutableArray alloc] init];
    
    FMResultSet *s = [[Table db] executeQuery:@"SELECT * FROM sector WHERE tipo='region' ORDER BY lat DESC"];
    while ([s next]) {
        
        [regiones addObject:[s resultDictionary]];
    }
    
    return [NSArray arrayWithArray:regiones];
}

+(NSDictionary *)findRegionByCodigo:(NSString *)codigo{
    FMResultSet *s = [[Table db] executeQuery:@"SELECT * FROM sector WHERE tipo='region' AND codigo = ?" withArgumentsInArray:[NSArray arrayWithObjects:codigo, nil]];
    if ([s next]) {
        
        return [s resultDictionary];
    }
    
    return nil;
}

+(NSArray *)findComunasByRegion:(NSString *)codigo{
    NSMutableArray *comunas=[[NSMutableArray alloc] init];
    
    FMResultSet *s = [[Table db] executeQuery:@"SELECT sector.* FROM sector\
                      LEFT JOIN sector AS s1 ON sector.sector_padre_codigo = s1.codigo\
                      LEFT JOIN sector AS s2 ON s1.sector_padre_codigo = s2.codigo\
                      WHERE sector.tipo='comuna' AND s2.codigo = ? ORDER BY nombre_ascii ASC" withArgumentsInArray:[NSArray arrayWithObjects:codigo, nil]];

    while ([s next]) {
        
        [comunas addObject:[s resultDictionary]];
    }
    
    
    
    return [NSArray arrayWithArray:comunas];
}

+(NSDictionary *)findComunaByCodigo:(NSString *)codigo{
    FMResultSet *s = [[Table db] executeQuery:@"SELECT * FROM sector WHERE tipo='comuna' AND codigo = ?" withArgumentsInArray:[NSArray arrayWithObjects:codigo, nil]];
    if ([s next]) {
        
        return [s resultDictionary];
    }
    
    return nil;
}

@end
