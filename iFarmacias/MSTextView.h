//
//  MSTextView.h
//  iFarmacias
//
//  Created by Nicolas Silva on 15-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GCPlaceholderTextView/GCPlaceholderTextView.h>

@interface MSTextView : GCPlaceholderTextView

@end
