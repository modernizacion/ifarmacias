//
//  MSFarmaciaAnnotation.m
//  iFarmacias
//
//  Created by Nicolas Silva on 14-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "MSFarmaciaAnnotation.h"

@implementation MSFarmaciaAnnotation

-(NSString *)groupTag{
    if (self.farmacia.estaAbierta) {
        return @"abierta";
    }else{
        return @"cerrada";
    }
}

@end
