//
//  Farmacia.m
//  iFarmacias
//
//  Created by Nicolas Silva on 27-11-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "Farmacia.h"

@interface Farmacia()

-(void)calcularEstaAbierta;
@property(nonatomic,strong)NSDate *estaAbiertaUpdatedAt;

@end

@implementation Farmacia

@synthesize estaAbierta=_estaAbierta;

-(BOOL)estaAbierta{
    NSDate *now=[NSDate date];
    
    if (now.timeIntervalSince1970 - self.estaAbiertaUpdatedAt.timeIntervalSince1970 > 60) {
        [self calcularEstaAbierta];
        self.estaAbiertaUpdatedAt=now;
    }
    
    return _estaAbierta;
    
}

-(void)calcularEstaAbierta{
    NSCalendar *calendar=[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    calendar.timeZone=[NSTimeZone timeZoneWithName:@"America/Santiago"];
    
    NSDateComponents *components = [calendar components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[NSDate date]];
    
    NSInteger horaApertura=[self.horarioApertura substringWithRange:NSMakeRange(0, 2)].integerValue;
    NSInteger minutoApertura=[self.horarioApertura substringWithRange:NSMakeRange(3, 2)].integerValue;
    NSInteger horaCierre=[self.horarioCierre substringWithRange:NSMakeRange(0, 2)].integerValue;
    NSInteger minutoCierre=[self.horarioCierre substringWithRange:NSMakeRange(3, 2)].integerValue;
    
    if (horaApertura > horaCierre || (horaApertura==horaCierre && minutoApertura > minutoCierre))
        horaCierre=horaCierre+24;
    
    
    if (horaApertura == horaCierre && minutoApertura==minutoCierre)
        _estaAbierta=YES;
    else if (components.hour < horaApertura || (components.hour==horaApertura && components.minute < minutoApertura))
        _estaAbierta= NO;
    else if (components.hour > horaCierre || (components.hour==horaCierre && components.minute > minutoCierre))
        _estaAbierta= NO;
    else
        _estaAbierta= YES;
}

@end
