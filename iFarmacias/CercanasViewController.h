//
//  CercanasViewController.h
//  iFarmacias
//
//  Created by Nicolas Silva on 03-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface CercanasViewController : UIViewController <MKMapViewDelegate,UITabBarControllerDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,strong) CLLocation *centerLocation;
@property(nonatomic,assign) BOOL panToUserLocation;
@end
