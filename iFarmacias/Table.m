//
//  Table.m
//  iFarmacias
//
//  Created by Nicolas Silva on 10-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "Table.h"

@interface Table(){
}
@end

@implementation Table


+(FMDatabase *)db{
    
    static FMDatabase *db=nil;
    
    if (!db) {
        NSString *dbPath = [[NSBundle mainBundle] pathForResource:@"ifarmacias" ofType:@"sqlite"];
        
        db = [FMDatabase databaseWithPath:dbPath];
        if(![self.db open]){
            NSLog(@"Could not open database.");
            return nil;
        }
    }
    
    return db;
}

@end
