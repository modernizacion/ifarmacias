//
//  FarmaciaTable.h
//  iFarmacias
//
//  Created by Nicolas Silva on 10-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FarmaciaTable : NSObject

+(void)findFarmaciasWithBlock:(void(^)(NSArray *, NSError *))block;
+(void)findFarmaciasByComuna:(NSString *)nombre withBlock:(void(^)(NSArray *, NSError *))block;

@end
