//
//  ListadoFarmaciasCell.h
//  iFarmacias
//
//  Created by Nicolas Silva on 11-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSCell.h"

@interface ListadoFarmaciasCell : MSCell

@property(nonatomic,weak) IBOutlet UILabel *nombre;
@property(nonatomic,weak) IBOutlet UILabel *direccion;


@end
