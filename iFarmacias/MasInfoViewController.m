//
//  MasInfoViewController.m
//  iFarmacias
//
//  Created by Nicolas Silva on 17-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "MasInfoViewController.h"

@interface MasInfoViewController ()
- (IBAction)modernizacionLinkButtonTouched:(id)sender;

@end

@implementation MasInfoViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
}

- (IBAction)modernizacionLinkButtonTouched:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.modernizacion.gob.cl"]];
    
}
@end
