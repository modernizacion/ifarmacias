//
//  ComunasViewController.m
//  iFarmacias
//
//  Created by Nicolas Silva on 03-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "SectoresViewController.h"
#import "SectorTable.h"
#import "ListadoRegionesViewController.h"
#import "ListadoComunasViewController.h"
#import "ListadoFarmaciasViewController.h"
#import <Parse/Parse.h>

@interface SectoresViewController ()
- (IBAction)searchButtonTouched:(id)sender;
@property (weak, nonatomic) IBOutlet UITableViewCell *regionCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *comunaCell;
@property (nonatomic,strong)NSDictionary *selectedRegion;
@property (nonatomic,strong)NSDictionary *selectedComuna;
@end

@implementation SectoresViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
        NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
        if ([userDefaults stringForKey:@"region_codigo"]) {
            self.selectedRegion=[SectorTable findRegionByCodigo:[userDefaults stringForKey:@"region_codigo"]];
        }
        if ([userDefaults stringForKey:@"comuna_codigo"]) {
            self.selectedComuna=[SectorTable findComunaByCodigo:[userDefaults stringForKey:@"comuna_codigo"]];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    
    if (self.selectedRegion) {
        self.regionCell.textLabel.text=self.selectedRegion[@"nombre"];
    }
    
    if (self.selectedComuna) {
        self.comunaCell.textLabel.text=self.selectedComuna[@"nombre"];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        [self performSegueWithIdentifier:@"ListadoRegiones" sender:self];
    }else if (indexPath.section==1){
        if (self.selectedRegion) {
            [self performSegueWithIdentifier:@"ListadoComunas" sender:self];
        }else{
            [[[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debe seleccionar una región." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        }
        
    }
    
}

#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    // Configure the cell...
    if (indexPath.section==0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellRegiones" forIndexPath:indexPath];
        cell.textLabel.text=self.selectedRegion[@"nombre"];
    }else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellComunas" forIndexPath:indexPath];
        cell.textLabel.text=self.selectedComuna[@"nombre"];
    }
    
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return @"Región";
    }else{
        return @"Comuna";
    }
    
}
 */

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

-(void)ListadoRegionesViewController:(ListadoRegionesViewController *)controller didSelectRegion:(NSDictionary *)region{
    self.selectedRegion=region;
    self.selectedComuna=nil;
    self.regionCell.textLabel.text=region[@"nombre"];
    self.comunaCell.textLabel.text=@"Seleccionar...";
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    [userDefaults setObject:region[@"codigo"] forKey:@"region_codigo"];
    //[self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)ListadoComunasViewController:(ListadoComunasViewController *)controller didSelectComuna:(NSDictionary *)comuna{
    self.selectedComuna=comuna;
    self.comunaCell.textLabel.text=comuna[@"nombre"];
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    [userDefaults setObject:comuna[@"codigo"] forKey:@"comuna_codigo"];
    //[self.tableView reloadData];
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ListadoRegiones"]) {
        ListadoRegionesViewController *viewController=(ListadoRegionesViewController *)segue.destinationViewController;
        [viewController setDelegate:self];
        [viewController setSelectedRegion:self.selectedRegion];
    }else if([segue.identifier isEqualToString:@"ListadoComunas"]){
        ListadoComunasViewController *viewController=(ListadoComunasViewController *)segue.destinationViewController;
        [viewController setDelegate:self];
        [viewController setSelectedRegion:self.selectedRegion];
        [viewController setSelectedComuna:self.selectedComuna];
    }else{
        ListadoFarmaciasViewController *viewController=(ListadoFarmaciasViewController *)segue.destinationViewController;
        [viewController setRegion:self.selectedRegion];
        [viewController setComuna:self.selectedComuna];
    }
    
}



- (IBAction)searchButtonTouched:(id)sender {
    if (self.selectedRegion && self.selectedComuna) {
        [self performSegueWithIdentifier:@"ListadoFarmacias" sender:self];
    }else{
        [[[UIAlertView alloc] initWithTitle:@"Atención" message:@"Debe seleccionar una región y comuna." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    }
}
@end
