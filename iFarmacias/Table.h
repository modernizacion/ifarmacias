//
//  Table.h
//  iFarmacias
//
//  Created by Nicolas Silva on 10-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDatabase.h>

@interface Table : NSObject

+(FMDatabase *)db;

@end
