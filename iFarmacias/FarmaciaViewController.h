//
//  FarmaciaViewController.h
//  iFarmacias
//
//  Created by Nicolas Silva on 14-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "EnviarDenunciaViewController.h"
#import "Farmacia.h"

@interface FarmaciaViewController : UIViewController <MKMapViewDelegate,EnviarDenunciaViewControllerDelegate>

@property(nonatomic,strong) Farmacia *farmacia;

@end
