//
//  ListadoFarmaciasViewController.m
//  iFarmacias
//
//  Created by Nicolas Silva on 03-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "ListadoFarmaciasViewController.h"
#import "FarmaciaTable.h"
#import "ListadoFarmaciasCell.h"
#import "FarmaciaViewController.h"
#import "CercanasViewController.h"
#import "NSDate+Extension.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface ListadoFarmaciasViewController ()
@property (weak, nonatomic) IBOutlet UILabel *sectorLabel;
@property(nonatomic,strong)NSArray *farmacias;
@property(nonatomic,assign)BOOL noResults;
@property (nonatomic, strong) NSDate *dateUpdated;
-(void)refreshTableView;
@end

@implementation ListadoFarmaciasViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.sectorLabel.text=[NSString stringWithFormat:@"%@, %@",self.region[@"nombre"],self.comuna[@"nombre"]];
    
    //[self refreshTableView];
    

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //Refrescamos solo cuando no ha habido refresco antes, o cuando la ultima actualizacion fue el dia anterior.
    if (self.dateUpdated==nil) {
        [self refreshTableView];
    }else{
        NSDateComponents *componentsToday = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate chileanLabourDate]];
        NSDateComponents *componentsUpdated = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:self.dateUpdated];
        if ([componentsToday day]!=[componentsUpdated day]) {
            [self refreshTableView];
        }
    }
}

-(void)refreshTableView{
    [SVProgressHUD showWithStatus:@"Cargando farmacias"];
    [FarmaciaTable findFarmaciasByComuna:self.comuna[@"nombre"] withBlock:^(NSArray *farmacias, NSError *error) {
        if (error) {
            [SVProgressHUD showErrorWithStatus:@"Problemas en la conexión"];
        }else{
            self.farmacias=farmacias;
            if ([self.farmacias count]==0) {
                self.noResults=YES;
            }
            [self.tableView reloadData];
            self.dateUpdated=[NSDate chileanLabourDate];
            [SVProgressHUD showSuccessWithStatus:@"Completado"];
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.noResults) {
        return 3;
    }else{
        return [self.farmacias count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.farmacias count]==0) {
        UITableViewCell *cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NoResultsCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if(indexPath.row==1){
            cell.textLabel.text=@"No hay farmacias.";
        }
        return cell;
    }else{
        static NSString *CellIdentifier = @"Cell";
        ListadoFarmaciasCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        Farmacia *farmacia=self.farmacias[indexPath.row];
        cell.nombre.text=[NSString stringWithFormat:@"Farmacia %@",farmacia.nombre];
        cell.direccion.text=farmacia.direccion;
        
        if (farmacia.estaAbierta) {
            cell.nombre.textColor=[UIColor blackColor];
            cell.direccion.textColor=[UIColor blackColor];
        }else{
            cell.nombre.textColor=[UIColor lightGrayColor];
            cell.direccion.textColor=[UIColor lightGrayColor];
        }
        
        if (indexPath.row%2==0) {
            cell.backgroundColor=[UIColor whiteColor];
        }else{
            cell.backgroundColor=[UIColor colorWithRed:249.0/255.0 green:252.0/255.0 blue:255.0/255.0 alpha:1.0];
        }
        
        
        return cell;
    }
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"Farmacia"]) {
        FarmaciaViewController *viewController=[segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        viewController.farmacia=self.farmacias[indexPath.row];
    }else if ([segue.identifier isEqualToString:@"Mapa"]){
        CercanasViewController *viewController=[segue destinationViewController];
        viewController.panToUserLocation=NO;
        viewController.centerLocation=[[CLLocation alloc] initWithLatitude:[self.comuna[@"lat"] doubleValue] longitude:[self.comuna[@"lng"] doubleValue]];
    }
    
}


@end
