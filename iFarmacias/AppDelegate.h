//
//  AppDelegate.h
//  iFarmacias
//
//  Created by Nicolas Silva on 03-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
