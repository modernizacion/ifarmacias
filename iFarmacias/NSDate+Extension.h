//
//  MSDate.h
//  iFarmacias
//
//  Created by Nicolas Silva on 11-11-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extension)
+(instancetype)chileanLabourDate;
@end
