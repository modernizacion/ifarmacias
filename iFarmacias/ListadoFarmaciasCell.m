//
//  ListadoFarmaciasCell.m
//  iFarmacias
//
//  Created by Nicolas Silva on 11-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import "ListadoFarmaciasCell.h"

@implementation ListadoFarmaciasCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
