//
//  SectorTable.h
//  iFarmacias
//
//  Created by Nicolas Silva on 04-10-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Table.h"

@interface SectorTable : NSObject

+(NSArray*)findRegiones;
+(NSDictionary *)findRegionByCodigo:(NSString *)codigo;

+(NSArray*)findComunasByRegion:(NSString *)codigo;
+(NSDictionary *)findComunaByCodigo:(NSString *)codigo;

@end
