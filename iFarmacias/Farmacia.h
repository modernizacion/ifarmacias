//
//  Farmacia.h
//  iFarmacias
//
//  Created by Nicolas Silva on 27-11-13.
//  Copyright (c) 2013 Nicolas Silva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Farmacia : NSObject

@property(nonatomic,strong)NSString *nombre;
@property(nonatomic,strong)NSString *direccion;
@property(nonatomic,strong)NSString *telefono;
@property(nonatomic,strong)NSString *horarioApertura;
@property(nonatomic,strong)NSString *horarioCierre;
@property(nonatomic,strong)NSString *comuna;
@property(nonatomic,assign)CLLocationCoordinate2D coordenadas;
@property(nonatomic,readonly,assign)BOOL estaAbierta;

@end
