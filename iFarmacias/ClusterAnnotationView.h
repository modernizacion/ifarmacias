//
//  ClusterAnnotationView.h
//  iTransantiago
//
//  Created by Nicolas Silva on 20-02-13.
//
//

#import <MapKit/MapKit.h>

@interface ClusterAnnotationView : MKAnnotationView

@property (nonatomic,assign) int numero;
@property (nonatomic,strong) UIColor *color;

@end
