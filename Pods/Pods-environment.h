
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// FMDB
#define COCOAPODS_POD_AVAILABLE_FMDB
#define COCOAPODS_VERSION_MAJOR_FMDB 2
#define COCOAPODS_VERSION_MINOR_FMDB 1
#define COCOAPODS_VERSION_PATCH_FMDB 0

// FMDB/common
#define COCOAPODS_POD_AVAILABLE_FMDB_common
#define COCOAPODS_VERSION_MAJOR_FMDB_common 2
#define COCOAPODS_VERSION_MINOR_FMDB_common 1
#define COCOAPODS_VERSION_PATCH_FMDB_common 0

// FMDB/standard
#define COCOAPODS_POD_AVAILABLE_FMDB_standard
#define COCOAPODS_VERSION_MAJOR_FMDB_standard 2
#define COCOAPODS_VERSION_MINOR_FMDB_standard 1
#define COCOAPODS_VERSION_PATCH_FMDB_standard 0

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 10
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 0

// GCPlaceholderTextView
#define COCOAPODS_POD_AVAILABLE_GCPlaceholderTextView
#define COCOAPODS_VERSION_MAJOR_GCPlaceholderTextView 1
#define COCOAPODS_VERSION_MINOR_GCPlaceholderTextView 0
#define COCOAPODS_VERSION_PATCH_GCPlaceholderTextView 1

// MKMapViewZoom
#define COCOAPODS_POD_AVAILABLE_MKMapViewZoom
#define COCOAPODS_VERSION_MAJOR_MKMapViewZoom 1
#define COCOAPODS_VERSION_MINOR_MKMapViewZoom 0
#define COCOAPODS_VERSION_PATCH_MKMapViewZoom 0

// OCMapView
#define COCOAPODS_POD_AVAILABLE_OCMapView
#define COCOAPODS_VERSION_MAJOR_OCMapView 0
#define COCOAPODS_VERSION_MINOR_OCMapView 0
#define COCOAPODS_VERSION_PATCH_OCMapView 1

// Parse-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Parse_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Parse_iOS_SDK 1
#define COCOAPODS_VERSION_MINOR_Parse_iOS_SDK 2
#define COCOAPODS_VERSION_PATCH_Parse_iOS_SDK 16

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 0
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 0

// TestFlightSDK
#define COCOAPODS_POD_AVAILABLE_TestFlightSDK
#define COCOAPODS_VERSION_MAJOR_TestFlightSDK 2
#define COCOAPODS_VERSION_MINOR_TestFlightSDK 2
#define COCOAPODS_VERSION_PATCH_TestFlightSDK 0

